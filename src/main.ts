import { createApp } from 'vue'
import './style.scss'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.css"
import { OhVueIcon,addIcons } from 'oh-vue-icons'
import { FaWindowClose } from "oh-vue-icons/icons";

addIcons(FaWindowClose);

const app=createApp(App);
app.component('v-icon',OhVueIcon)
app.mount('#app')
