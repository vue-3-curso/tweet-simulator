import {v4 as uuidv4} from 'uuid'
import {size,remove} from 'lodash'
import {TWEETS} from '../utils/constants'
import { ITweet } from '../interfaces/ITweet'

export function saveTweetApi(username:string,body:string):void{
    const tweets:Array<ITweet> =getTweetsApi();

    const newTweet:ITweet={
        id:uuidv4(),
        username:username,
        body:body,
        createdAt:new Date()
    }

    if (size(tweets)==0){
        const arrayTemp:Array<ITweet> =[];
        arrayTemp.push(newTweet);
        localStorage.setItem(TWEETS,JSON.stringify(arrayTemp));
    }else{
        tweets.push(newTweet)
        localStorage.setItem(TWEETS,JSON.stringify(tweets));
    }

    
}

export function getTweetsApi():Array<ITweet>{
    const tweets=localStorage.getItem(TWEETS);
    if(tweets){
        return (JSON.parse(tweets)).reverse();
    }
    return [];
}

export function deleteTweetApi(id:string):void{
    let tweets:Array<ITweet> =getTweetsApi();
    remove(tweets,(tweet:ITweet)=>{
        return tweet.id===id;
    })
    localStorage.setItem(TWEETS,JSON.stringify(tweets));
}