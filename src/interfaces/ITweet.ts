export interface ITweet{
    id:string,
    username:string,
    body:string,
    createdAt:Date
}